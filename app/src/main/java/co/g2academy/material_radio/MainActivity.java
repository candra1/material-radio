package co.g2academy.material_radio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    RadioGroup radioGroup;
    RadioButton radio_button_1, radio_button_2, radio_button_3, radio_button_4, radio_button_5;
    Integer checkedRadioButtonId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radio_button_1 = (RadioButton) findViewById(R.id.radio_button_1);
        radio_button_2 = (RadioButton) findViewById(R.id.radio_button_2);
        radio_button_3 = (RadioButton) findViewById(R.id.radio_button_3);
        radio_button_4 = (RadioButton) findViewById(R.id.radio_button_4);
        radio_button_5 = (RadioButton) findViewById(R.id.radio_button_5);
    }
}